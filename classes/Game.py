import operator

class TableTop:

    def __init__(self):

        #init the tableBoxes in matrix 4x4
        self.box = [[None] * 4] * 4
        #create instance of single pieces
        #all with height = Piece.longPiece, white
        self.piece1 = Piece(Piece.longPiece, Piece.pieceWithHole, Piece.squarePiece, Piece.whitePiece)
        self.piece2 = Piece(Piece.longPiece, Piece.pieceWithoutHole, Piece.squarePiece, Piece.whitePiece)
        self.piece3 = Piece(Piece.longPiece, Piece.pieceWithHole, Piece.circlePiece, Piece.whitePiece)
        self.piece4 = Piece(Piece.longPiece, Piece.pieceWithoutHole, Piece.circlePiece, Piece.whitePiece)

        #all with height = Piece.longPiece, black
        self.piece5 = Piece(Piece.longPiece, Piece.pieceWithHole, Piece.squarePiece, Piece.blackPiece)
        self.piece6 = Piece(Piece.longPiece, Piece.pieceWithoutHole, Piece.squarePiece, Piece.blackPiece)
        self.piece7 = Piece(Piece.longPiece, Piece.pieceWithHole, Piece.circlePiece, Piece.blackPiece)
        self.piece8 = Piece(Piece.longPiece, Piece.pieceWithoutHole, Piece.circlePiece, Piece.blackPiece)

        #all with height = Piece.shortPiece, white
        self.piece9 = Piece(Piece.shortPiece, Piece.pieceWithHole, Piece.squarePiece, Piece.whitePiece)
        self.piece10 = Piece(Piece.shortPiece, Piece.pieceWithoutHole, Piece.squarePiece, Piece.whitePiece)
        self.piece11 = Piece(Piece.shortPiece, Piece.pieceWithHole, Piece.circlePiece, Piece.whitePiece)
        self.piece12 = Piece(Piece.shortPiece, Piece.pieceWithoutHole, Piece.circlePiece, Piece.whitePiece)


        #all with height = Piece.shortPiece, black
        self.piece13 = Piece(Piece.shortPiece, Piece.pieceWithHole, Piece.squarePiece, Piece.blackPiece)
        self.piece14 = Piece(Piece.shortPiece, Piece.pieceWithoutHole, Piece.squarePiece, Piece.blackPiece)
        self.piece15 = Piece(Piece.shortPiece, Piece.pieceWithHole, Piece.circlePiece, Piece.blackPiece)
        self.piece16 = Piece(Piece.shortPiece, Piece.pieceWithoutHole, Piece.circlePiece, Piece.blackPiece)

    def checkQuadrusHorizontal(self):
        for i in range(4):
            #prepare list for _checkQuadrus
            listBoxes = self.box[i];
            print(listBoxes)
 
        return  

    
    #def __checkQuadrus(self,listPieces):
     #   pass
        

class Piece:
 
    longPiece  = 2
    shortPiece  = 1
    pieceWithHole = True 
    pieceWithoutHole = False
    squarePiece = "square"
    circlePiece = "circle"
    blackPiece  = "black"
    whitePiece  = "white"

    
    def __init__(self,height,hole,shape,color):

        if (height != self.longPiece and height != self.shortPiece):
            raise Exception("Bad prop height, must be "+str(self.longPiece)+" or "+str(self.shortPiece))

        if (type(hole) is not bool ):
            raise Exception("Bad prop hole, must be boolean")

        if (shape != self.circlePiece and shape != self.squarePiece):
            raise Exception("Bad prop shape, must be "+self.circlePiece+" or "+self.squarePiece)

        if (color != self.blackPiece and color != self.whitePiece):
            raise Exception("Bad prop color, must be "+self.blackPiece+" or "+self.whitePiece)


        self._height = height
        self._hole = hole
        self._shape = shape
        self._color = color
        self._placed = False
    def setPlaced(self):
        self._placed = true

    def getHeight(self):
        return self._height


    def getHole(self):
        return self._hole

    def getShape(self):
        return self._shape

    def getColor(self):
        return self._color

   